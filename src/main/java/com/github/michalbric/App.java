package com.github.michalbric;

import com.github.michalbric.controller.HealthController;
import com.github.michalbric.controller.UserController;
import com.github.michalbric.controller.WSController;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class App {

    private UserController userController;
    private WSController wsController;

    @Inject
    public App(@Named("ws-controller") WSController wsController, @Named("user-controller") UserController userController) {
        this.userController = userController;
        this.wsController = wsController;
    }

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new BaseEntities());
        App app = injector.getInstance(App.class);

        Vertx vertx = Vertx.vertx();
        Router restRouter = Router.router(vertx);

        restRouter.route("/health").handler(HealthController::handleHealth);
        restRouter.route("/login").handler(app::login);
        restRouter.route(HttpMethod.POST, "/logout").handler(app::logout);

        vertx.createHttpServer().requestHandler(restRouter::accept).listen(8081);
        vertx.createHttpServer().websocketHandler(app::connection).listen(8091);
    }

    private void login(final RoutingContext routingContext) {
        userController.handleLogin(routingContext);
    }

    private void logout(final RoutingContext routingContext) {
        userController.handleLogout(routingContext);
    }

    private void connection(final ServerWebSocket serverWebSocket) {
        wsController.handleConnection(serverWebSocket);
    }
}
