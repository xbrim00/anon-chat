package com.github.michalbric.user;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.github.michalbric.controller.util.HttpConstants;
import com.github.michalbric.controller.util.WebUtil;
import com.github.michalbric.name.MasterNameActor;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashMap;
import java.util.Map;

public class UserLoginWebActor extends AbstractLoggingActor {

    /*** Implementation ***/
    private ActorRef masterNameActor;
    //private UserController userController;
    private Map<String, RoutingContext> transactions;

    private UserLoginWebActor(ActorRef masterNameRef) {
        this.masterNameActor = masterNameRef;
        //this.userController = userController;
    }

    @Override
    public void preStart() {
        //masterNameActor = App.MASTER_NAME_ACTOR;
        transactions = new HashMap<>();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(MasterUserActor.LoginRequest.class, this::handleLoginRequest)
                .match(MasterUserActor.LogoutRequest.class, this::handleLogoutRequest)
                .match(MasterNameActor.NameResponse.class, this::handleNameResponse)
                .build();
    }

    private void handleLoginRequest(final MasterUserActor.LoginRequest loginRequest) {
        String transactionID = loginRequest.getTransactionID();
        transactions.put(transactionID, loginRequest.getContext());
        final String userID = generatedUserID();
        masterNameActor.tell(MasterNameActor.NameRequest.forUser(userID, transactionID), self());
    }

    private void handleLogoutRequest(final MasterUserActor.LogoutRequest logoutRequest) {
        masterNameActor.tell(MasterNameActor.DropUser.ofID(logoutRequest.getUserID()), self());
    }

    private void handleNameResponse(final MasterNameActor.NameResponse nameResponse) {
        String transactionID = nameResponse.getTransactionID();
        if (!transactions.containsKey(transactionID)) {
            return;
        }
        RoutingContext rc = transactions.remove(transactionID);
        WebUtil.respondWithJson(rc, HttpConstants.STATUS_OK, WebUtil.JSONLoginResponse.of(nameResponse.getName(), nameResponse.getUserID()));
    }

    private String generatedUserID() {
        return "uid-" + RandomStringUtils.randomAlphanumeric(50);
    }

    public static Props props(final ActorRef masterNameRef) {
        return Props.create(UserLoginWebActor.class, () -> new UserLoginWebActor(masterNameRef));
    }
}
