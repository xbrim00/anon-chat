package com.github.michalbric.user;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import io.vertx.ext.web.RoutingContext;
import lombok.Data;

public class MasterUserActor extends AbstractLoggingActor {

    // this will be called from the user controller..
    /*** Protocol ***/
    @Data(staticConstructor = "of")
    public static class LoginRequest {

        private final RoutingContext context;
        private final String transactionID;
    }
    @Data(staticConstructor = "by")
    public static class LogoutRequest {

        private final String userID;
    }
    /*** Implementation ***/
    private ActorRef loginActor;
    private final ActorRef masterNameActor;

    private MasterUserActor(ActorRef masterNameActor) {
        this.masterNameActor = masterNameActor;
    }

    @Override
    public void preStart() {
        loginActor = getContext().getSystem().actorOf(UserLoginWebActor.props(masterNameActor));
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(LoginRequest.class, msg -> loginActor.tell(msg, self()))
                .match(LogoutRequest.class, msg -> loginActor.tell(msg, self()))
                .build();
    }

    public static Props props(final ActorRef masterNameActor) {
        return Props.create(MasterUserActor.class, () -> new MasterUserActor(masterNameActor));
    }
}
