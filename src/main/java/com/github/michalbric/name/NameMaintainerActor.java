package com.github.michalbric.name;

import akka.actor.*;
import java.util.*;

import static com.github.michalbric.name.InMemoryNameStoreActor.*;
import static com.github.michalbric.name.MasterNameActor.*;

// This actor is responsible for maintaining the current active users and their names as well as
// answer requests for new names and clearing old names
// as delegates the persistence actions to storage actor
public class NameMaintainerActor extends AbstractActorWithStash {


    /*** Implementation ***/
    private ActorRef storageActor;
    private Random random;
    private List<String> availableNames;
    private Map<String, String> userIDToName;

    @Override
    public void preStart() throws Exception {
        random = new Random();
        userIDToName = new HashMap<>();
        storageActor = getContext().actorOf(InMemoryNameStoreActor.props(), "storage-actor-01");
        storageActor.tell(AllNamesRequest.create(), self());
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(AllNamesResponse.class, msg -> {
                    initializeNames(msg);
                    getContext().become(initializedReceive());
                    System.out.println("Initialization done, unstashing");
                    unstashAll();
                })
                .matchAny(msg ->  {
                    System.out.println("Stashing..");
                    stash();
                })
                .build();
    }

    private Receive initializedReceive() {
        return receiveBuilder()
                .match(NameRequest.class, this::respondWithName)
                .match(DropUser.class, this::dropUser)
                .match(NameVerificationRequest.class, this::verifyUser)
                .build();
    }

    private void initializeNames(final AllNamesResponse allNamesResponse) {
        this.availableNames = new ArrayList<>(allNamesResponse.getNames());
    }

    private void respondWithName(final NameRequest nameRequest) {
        String userID = nameRequest.getUserID();
        String transactionID = nameRequest.getTransactionID();
        getSender().tell(NameResponse.of(randomName(userID), userID, transactionID), self());
    }

    private void dropUser(final DropUser dropUser) {
        String userID = dropUser.getUserID();
        if (!userIDToName.containsKey(userID)) {
            return;
        }
        String freedName = userIDToName.remove(userID);
        availableNames.add(freedName);
        storageActor.tell(ClearName.of(freedName), self());
        System.out.println("Cleared userID " + userID);
    }

    private void verifyUser(final NameVerificationRequest verificationRequest) {
        String userID = verificationRequest.getUserID();
        String userName = verificationRequest.getName();
        boolean success = userIDToName.containsKey(userID) && userName.equals(userIDToName.get(userID));
        getSender().tell(NameVerificationResponse.of(verificationRequest.getTransactionID(), userID, userName, success), self());
    }

    private String randomName(final String userID) {
        String chosenName = availableNames.remove(random.nextInt(availableNames.size()));
        String previousName = userIDToName.get(userID);
        if (previousName != null) {
            availableNames.add(previousName);
        }
        userIDToName.put(userID, chosenName);
        storageActor.tell(AssignName.of(chosenName, userID), self());
        System.out.println("Assigned name " + chosenName + " to userID " + userID);
        return chosenName;
    }

    static Props props() {
        return Props.create(NameMaintainerActor.class);
    }
}
