package com.github.michalbric.name;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import lombok.Data;

public class MasterNameActor extends AbstractLoggingActor {

    /*** Protocol ***/
    @Data(staticConstructor = "forUser")
    public static class NameRequest {
        private final String userID;
        private final String transactionID;
    }

    @Data(staticConstructor = "of")
    public static class NameResponse {
        private final String name;
        private final String userID;
        private final String transactionID;
    }
    @Data(staticConstructor = "ofID")
    public static class DropUser { private final String userID; }

    @Data(staticConstructor = "of")
    public static class NameVerificationRequest {
        private final String userID;
        private final String name;
        private final String transactionID;
    }
    @Data(staticConstructor = "of")
    public static class NameVerificationResponse {
        private final String transactionID;
        private final String userID;
        private final String name;
        private final boolean success;
    }

    /*** Implementation ***/
    private ActorRef nameMaintainerActor;

    @Override
    public void preStart() {
        nameMaintainerActor = getContext().actorOf(NameMaintainerActor.props(), "name-maintainer-01");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(NameRequest.class, this::forwardNameRequest)
                .match(DropUser.class, this::forwardsUserDrop)
                .match(NameVerificationRequest.class, this::forwardVerificationRequest)
                .build();
    }

    private void forwardNameRequest(final NameRequest req) {
        nameMaintainerActor.tell(req, sender());
    }

    private void forwardsUserDrop(final DropUser dropUser) {
        nameMaintainerActor.tell(dropUser, self());
    }

    private void forwardVerificationRequest(final NameVerificationRequest req) {
        nameMaintainerActor.tell(req, sender());
    }

    public static Props props() {
        return Props.create(MasterNameActor.class);
    }
}
