package com.github.michalbric.name;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import lombok.Data;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

class InMemoryNameStoreActor extends AbstractLoggingActor {

    /*** Protocol ***/
    @Data(staticConstructor = "create")
    static class AllNamesRequest {}

    @Data(staticConstructor = "of")
    static class AllNamesResponse { private final Set<String> names; }

    @Data(staticConstructor = "of")
    static class AssignName {
        private final String name;
        private final String userID;
    }

    @Data(staticConstructor = "of")
    static class ClearName {
        private final String name;
    }

    /*** Implementation ***/
    private static final String NAMES_FILE_PATH = "./src/main/resources/names/names.txt";

    // Set of collections, sacrifices memory for fast operations
    private Map<String, String> nameToUserID;

    @Override
    public void preStart() throws Exception {
        nameToUserID = loadEmptyNames();
        log().info("Empty names loaded)");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(AllNamesRequest.class, req -> respondWithAllNames())
                .match(AssignName.class, this::assignName)
                .match(ClearName.class, req -> clearName(req.getName()))
                .build();
    }

    private void respondWithAllNames() {
        sender().tell(AllNamesResponse.of(nameToUserID.keySet()), self());
        log().info("Responsded with all names");
    }

    private void assignName(final AssignName msg) {
        String name = msg.getName();
        String userID = msg.getUserID();

        dropUser(userID);
        nameToUserID.put(name, userID);
        log().debug("assigned name " + name + " to userID " + userID);
    }

    private void dropUser(final String userID) {
        nameToUserID.values().remove(userID);
        log().debug("dropped userID" + userID);
    }

    private void clearName(final String name) {
        nameToUserID.replace(name, null);
        log().debug("cleared name " + name);
    }
    private Map<String, String> loadEmptyNames() throws Exception {
        try (Stream<String> nameLines = Files.lines(Paths.get(NAMES_FILE_PATH))) {
            Map<String, String> result = new HashMap<>();
            nameLines
                    .map(String::trim)
                    .filter(s -> !s.isEmpty())
                    .forEach(name -> result.put(name, null));
            return result;
        }
    }

    static Props props() {
        return Props.create(InMemoryNameStoreActor.class);
    }

}
