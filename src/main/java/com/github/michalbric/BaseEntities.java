package com.github.michalbric;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.github.michalbric.controller.UserController;
import com.github.michalbric.controller.WSController;
import com.github.michalbric.gateway.WebSocketGatewayActor;
import com.github.michalbric.name.MasterNameActor;
import com.github.michalbric.user.MasterUserActor;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

public class BaseEntities extends AbstractModule {

    @Inject
    @Singleton
    @Provides
    @Named("master-name-actor")
    public ActorRef masterNameActor(@Named("base-system") ActorSystem system) {
        return system.actorOf(MasterNameActor.props(), "master-name-actor");
    }

    @Inject
    @Singleton
    @Provides
    @Named("master-user-actor")
    public ActorRef masterUserActor(@Named("base-system") ActorSystem system,
                                    @Named("master-name-actor") ActorRef masterNameActor) {
        return system.actorOf(MasterUserActor.props(masterNameActor), "master-user-actor");
    }

    @Singleton @Provides @Named("base-system")
    public ActorSystem baseSystem() {
        return ActorSystem.create("anon-system");
    }

    @Inject @Singleton @Provides
    @Named("websocket-gateway-actor")
    public ActorRef websocketGatewayActor(@Named("base-system") ActorSystem system,
                                          @Named("master-name-actor") ActorRef masterNameActor) {
        return system.actorOf(WebSocketGatewayActor.props(masterNameActor), "websocket-gateway");
    }

    @Provides
    @Singleton
    @Named("ws-controller")
    public WSController wsController(@Named("websocket-gateway-actor") ActorRef gatewayRef) {
        return new WSController(gatewayRef);
    }

    @Provides
    @Singleton
    @Named("user-controller")
    public UserController userController(@Named("master-user-actor") ActorRef masterUserRef) {
        return new UserController(masterUserRef);
    }
}
