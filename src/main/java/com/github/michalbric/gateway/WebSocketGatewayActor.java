package com.github.michalbric.gateway;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.github.michalbric.model.ChatMessage;
import com.github.michalbric.name.MasterNameActor;
import com.github.michalbric.room.MasterRoomActor;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.JsonObject;
import lombok.Data;

import java.util.*;

public class WebSocketGatewayActor extends AbstractLoggingActor {

    /*** Protocol ***/
    @Data(staticConstructor = "of")
    public static class WebSocketConnection {
        private final ServerWebSocket ws;
    }

    @Data(staticConstructor = "of")
    private static class WebSocketEvent {
        private final String textID;
        private final Buffer event;
    }

    @Data(staticConstructor = "by")
    private static class WebSocketCloseEvent {
        private final String textID;
    }

    @Data(staticConstructor = "of")
    public static class JoinEventMessage {
        private final String textID;
        private final String userName;
        private final String target;
    }

    @Data(staticConstructor = "of")
    public static class LeaveEventMessage {
        private final String textID;
        private final String target;
        private final boolean global;
    }
    @Data(staticConstructor = "of")
    public static class ChatEventMessage {
        private final String textID;
        private final String userName;
        private final String target;
        private final String text;
    }

    @Data(staticConstructor = "of")
    public static class DistributeRequest {
        private final Set<String> recipientTextIDs;
        private final ChatMessage message;
    }

    @Data(staticConstructor = "of")
    public static class DirectMessagesRequest {
        private final String recipientTextID;
        private final List<ChatMessage> messages;
    }

    /*** Implementation ***/
    private ActorRef masterNameActor;
    private ActorRef roomActor;
    private Map<String, UserSocket> verifiedUsers;
    private Map<String, ServerWebSocket> pendingWebSockets;

    private WebSocketGatewayActor(final ActorRef masterNameActor) {
        this.masterNameActor = masterNameActor;
    }

    @Override
    public void preStart() throws Exception {
        //masterNameActor = App.MASTER_NAME_ACTOR;
        roomActor = getContext().actorOf(MasterRoomActor.props(), "master-room");
        verifiedUsers = new HashMap<>();
        pendingWebSockets = new HashMap<>();

    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(WebSocketConnection.class, this::registerWebSocketConnection)
                .match(WebSocketEvent.class, this::handleWebSocketEvent)
                .match(WebSocketCloseEvent.class, this::handleWebSocketCloseEvent)
                .match(MasterNameActor.NameVerificationResponse.class, this::handleUserVerification)
                .match(DistributeRequest.class, this::distribute)
                .match(DirectMessagesRequest.class, this::sendMultipleDirectly)
                .build();
    }

    private void registerWebSocketConnection(final WebSocketConnection webSocketConnection) {
        ServerWebSocket ws = webSocketConnection.getWs();
        if (ws == null) {
            return;
        }
        ws.accept();
        String textHandlerID = ws.textHandlerID();
        pendingWebSockets.put(textHandlerID, ws);

        ws.handler(event -> self().tell(WebSocketEvent.of(textHandlerID, event), self()));
        ws.closeHandler(event -> self().tell(WebSocketCloseEvent.by(textHandlerID), self()));
        log().info("Accepted socket " + textHandlerID);
    }

    /*** Message handlers ***/

    private void handleWebSocketEvent(final WebSocketEvent webSocketEvent) {
        Optional<WebSocketAction> optWSAction = actionFromEvent(webSocketEvent);
        if (!optWSAction.isPresent()) {
            return;
        }
        WebSocketAction wsAction = optWSAction.get();
        if (wsAction.getType().equals(WebSocketActionType.VERIFY)) {
            masterNameActor.tell(
                    MasterNameActor.NameVerificationRequest.of(
                            wsAction.getUserID(), wsAction.getUserName(), webSocketEvent.getTextID()), self());
        } else {
            handleRoomAction(webSocketEvent.getTextID(), wsAction);
        }
        log().info("Received a ws action: " + wsAction);
    }

    private void handleWebSocketCloseEvent(final WebSocketCloseEvent webSocketCloseEvent) {
        String closingSocketTextID = webSocketCloseEvent.getTextID();
        pendingWebSockets.remove(closingSocketTextID);
        UserSocket removedUserSocket = verifiedUsers.remove(closingSocketTextID);
        if (removedUserSocket != null) {
            masterNameActor.tell(MasterNameActor.DropUser.ofID(removedUserSocket.getUserID()), self());
            roomActor.tell(LeaveEventMessage.of(closingSocketTextID, null, true), self());
        }
        log().info("Closing socket " + closingSocketTextID);
    }

    private void handleUserVerification(final MasterNameActor.NameVerificationResponse verification) {
        String textID = verification.getTransactionID();
        ServerWebSocket pendingWebSocket = pendingWebSockets.remove(textID);
        if (!verification.isSuccess() && pendingWebSocket != null) {
            pendingWebSocket.writeFinalTextFrame("{\"error\": \"incorrect verification, closing socket\"}");
            pendingWebSocket.close();
            return;
        }
        verifiedUsers.put(textID, UserSocket.of(pendingWebSocket, verification.getUserID(), verification.getName()));
        log().info("Verified user for socket ID " + textID);
    }

    private void distribute(final DistributeRequest req) {
        ChatMessage message = req.getMessage();
        String jsonStringPayload = JsonObject.mapFrom(message).encode();
        req.getRecipientTextIDs().stream()
            .filter(verifiedUsers::containsKey)
            .map(verifiedUsers::get)
            .map(UserSocket::getWebSocket)
            .forEach(websocket -> websocket.writeFinalTextFrame(jsonStringPayload));
    }

    private void sendMultipleDirectly(final DirectMessagesRequest req) {
        if (!verifiedUsers.containsKey(req.recipientTextID)) {
            return;
        }
        ServerWebSocket webSocket = verifiedUsers.get(req.recipientTextID).getWebSocket();
        req.messages.stream()
                .filter(Objects::nonNull)
                .map(msg -> JsonObject.mapFrom(msg).encode())
                .forEach(webSocket::writeFinalTextFrame);
    }

    private Optional<WebSocketAction> actionFromEvent(final WebSocketEvent wsEvent) {
        try {
            JsonObject eventJSON = new JsonObject(wsEvent.getEvent());
            return Optional.of(eventJSON.mapTo(WebSocketAction.class));
        } catch (Exception ex) {
            log().warning(ex.getMessage());
            return Optional.empty();
        }
    }

    /*** Helpers ***/

    private void handleRoomAction(final String textID, final WebSocketAction msgAction) {
        if (pendingWebSockets.containsKey(textID)) {
            pendingWebSockets.get(textID).writeFinalTextFrame("{\"error\": \"not verified\"}");
            return;
        }
        WebSocketActionType actionType = msgAction.getType();
        switch (actionType) {
            case JOIN:
                roomActor.tell(JoinEventMessage.of(textID, msgAction.userName, msgAction.getTarget()), self());
                break;
            case LEAVE:
                roomActor.tell(LeaveEventMessage.of(textID, msgAction.getTarget(), false), self());
                break;
            case MESSAGE:
                roomActor.tell(ChatEventMessage.of(textID, msgAction.getUserName(), msgAction.getTarget(), msgAction.getText()), self());
                break;
            default:
                log().info("Received an unknown verified message from " + msgAction.getUserName() + ": " + msgAction.getText());
        }

    }
    @Data(staticConstructor = "of")
    private static class UserSocket {
        private final ServerWebSocket webSocket;
        private final String userID;
        private final String name;
    }

    @Data(staticConstructor = "of")
    private static class WebSocketAction {
        private final WebSocketActionType type;
        private final String target;
        private final String text;
        private final String userID;
        private final String userName;
    }

    public static Props props(ActorRef masterNameActor) {
        return Props.create(WebSocketGatewayActor.class, () -> new WebSocketGatewayActor(masterNameActor));
    }
}
