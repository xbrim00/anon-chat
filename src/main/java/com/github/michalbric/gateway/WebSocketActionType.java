package com.github.michalbric.gateway;

public enum WebSocketActionType {
    VERIFY,
    JOIN,
    LEAVE,
    MESSAGE
}
