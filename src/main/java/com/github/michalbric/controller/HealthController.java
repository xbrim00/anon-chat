package com.github.michalbric.controller;

import com.github.michalbric.controller.util.HttpConstants;
import com.github.michalbric.controller.util.WebUtil;
import com.github.michalbric.model.Health;
import io.vertx.ext.web.RoutingContext;

public class HealthController {

    public static void handleHealth(final RoutingContext routingContext) {
        WebUtil.respondWithJson(routingContext, HttpConstants.STATUS_OK, Health.of("UP"));
    }
}
