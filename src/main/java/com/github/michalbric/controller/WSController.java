package com.github.michalbric.controller;

import akka.actor.ActorRef;
import com.github.michalbric.gateway.WebSocketGatewayActor;
import io.vertx.core.http.ServerWebSocket;

public class WSController {

    private final ActorRef gatewayActor;

    public WSController(final ActorRef gatewayActor) {
        this.gatewayActor = gatewayActor;
    }

    public void handleConnection(final ServerWebSocket webSocket) {
        gatewayActor.tell(WebSocketGatewayActor.WebSocketConnection.of(webSocket), ActorRef.noSender());
    }
}
