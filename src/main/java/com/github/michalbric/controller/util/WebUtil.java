package com.github.michalbric.controller.util;

import com.google.gson.Gson;
import io.vertx.ext.web.RoutingContext;
import lombok.Value;

import static com.github.michalbric.controller.util.HttpConstants.HEADER_APPLICATION_JSON;
import static com.github.michalbric.controller.util.HttpConstants.HEADER_CONTENT_TYPE;

public class WebUtil {

    private static final Gson gson = new Gson();

    public static void respondWithJson(final RoutingContext ctx, final int statusCode, final Object body) {
        ctx.response()
                .setStatusCode(statusCode)
                .putHeader(HEADER_CONTENT_TYPE, HEADER_APPLICATION_JSON)
                .end(gson.toJson(body));
    }

    public static void respondEmpty(final RoutingContext ctx, final int statusCode) {
        ctx.response()
                .setStatusCode(statusCode)
                .end();
    }

    @Value(staticConstructor = "of")
    public static class JSONLoginResponse {
        private final String userName;
        private final String userID;
    }

    @Value(staticConstructor = "with")
    public static class JSONErrorResponse {
        private final String reason;
    }
}
