package com.github.michalbric.controller.util;

public class HttpConstants {
    // Http status codes
    public static final int STATUS_OK = 200;
    public static final int STATUS_BAD_REQUEST = 400;

    // Headers
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_APPLICATION_JSON = "application/json";
}
