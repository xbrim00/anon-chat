package com.github.michalbric.controller;

import akka.actor.ActorRef;
import com.github.michalbric.controller.util.HttpConstants;
import com.github.michalbric.controller.util.WebUtil;
import com.github.michalbric.user.MasterUserActor;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

public class UserController {

    private ActorRef masterUserRef;

    public UserController(final ActorRef masterUserRef) {
        this.masterUserRef = masterUserRef;
    }

    public void handleLogin(final RoutingContext routingContext) {
        masterUserRef.tell(MasterUserActor.LoginRequest.of(routingContext, generatedTransactionID()), ActorRef.noSender());
    }

    private static String generatedTransactionID() {
        return "tid-" + RandomStringUtils.randomAlphanumeric(50);
    }

    public void handleLogout(RoutingContext routingContext) {
        routingContext.request().bodyHandler(bc -> {
            try {
                String userID = bc.toJsonObject().getString("userID");
                if (StringUtils.isEmpty(userID)) {
                    throw new RuntimeException("Missing userID");
                }
                masterUserRef.tell(MasterUserActor.LogoutRequest.by(userID), ActorRef.noSender());
                WebUtil.respondEmpty(routingContext, HttpConstants.STATUS_OK);
            } catch (Exception ex) {
                WebUtil.respondWithJson(routingContext, HttpConstants.STATUS_BAD_REQUEST,
                        WebUtil.JSONErrorResponse.with("Missing userID field in POST body"));
            }
        });
    }
}
