package com.github.michalbric.room;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import com.github.michalbric.gateway.WebSocketActionType;
import com.github.michalbric.gateway.WebSocketGatewayActor;
import com.github.michalbric.model.ChatMessage;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class SingleRoomActor extends AbstractLoggingActor {

    /*** Implementation ***/
    private final String name;
    private Map<String, RoomUser> users;

    public SingleRoomActor(final String name) {
        this.name = name;
        this.users = new HashMap<>();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(WebSocketGatewayActor.JoinEventMessage.class, this::handleJoin)
                .match(WebSocketGatewayActor.LeaveEventMessage.class, this::handleLeave)
                .match(WebSocketGatewayActor.ChatEventMessage.class, this::handleChat)
                .build();
    }

    private void handleJoin(final WebSocketGatewayActor.JoinEventMessage join) {
        if (!name.equals(join.getTarget())) {
            return;
        }
        String userName = join.getUserName();
        String textID = join.getTextID();
        RoomUser newUser = RoomUser.of(userName, textID);

        List<ChatMessage> currentUserJoins = users.values().stream()
                .map(ru -> ChatMessage.of(WebSocketActionType.JOIN, ru.getUserName(), name, null))
                .collect(Collectors.toList());
        users.put(textID, newUser);
        requestDistribution(WebSocketActionType.JOIN, userName, null);
        sender().tell(WebSocketGatewayActor.DirectMessagesRequest.of(textID, currentUserJoins), self());
    }

    private void handleLeave(final WebSocketGatewayActor.LeaveEventMessage leave) {
        if (!name.equals(leave.getTarget()) && !leave.isGlobal()) {
            return;
        }
        String textID = leave.getTextID();
        RoomUser leaver = users.remove(textID);
        if (leaver == null) {
            return;
        }
        requestDistribution(WebSocketActionType.LEAVE, leaver.getUserName(), null);
        informParentIfEmpty();
    }

    private void handleChat(final WebSocketGatewayActor.ChatEventMessage chat) {
        if (!name.equals(chat.getTarget())) {
            return;
        }
        String textID = chat.getTextID();
        String userName = chat.getUserName();
        if (!users.containsKey(textID)) {
            return;
        }
        requestDistribution(WebSocketActionType.MESSAGE, userName, chat.getText());
    }

    private void requestDistribution(final WebSocketActionType type, final String userName, final String text) {
        ChatMessage chatMessage = ChatMessage.of(type, userName, this.name, text);
        Set<String> recipients = users.values().stream()
                .map(RoomUser::getTextID)
                .collect(Collectors.toSet());
        sender().tell(WebSocketGatewayActor.DistributeRequest.of(recipients, chatMessage), self());
    }

    private void informParentIfEmpty() {
        if (!users.isEmpty()) {
            return;
        }
        getContext().getParent().tell(MasterRoomActor.RoomEmptyMessage.from(name), self());
    }

    public static Props props(final String name) {
        return Props.create(SingleRoomActor.class, name);
    }

    @Data(staticConstructor = "of")
    private static class RoomUser {
        private final String userName;
        private final String textID;
    }
}
