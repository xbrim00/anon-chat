package com.github.michalbric.room;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.github.michalbric.gateway.WebSocketGatewayActor;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class MasterRoomActor extends AbstractLoggingActor {

    /*** Protocol ***/
    @Data(staticConstructor = "from")
    static class RoomEmptyMessage {
        private final String name;
    }

    /*** Implementation ***/
    private Map<String, ActorRef> roomActors;

    @Override
    public void preStart() {
        roomActors = new HashMap<>();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(WebSocketGatewayActor.JoinEventMessage.class, this::forwardJoin)
                .match(WebSocketGatewayActor.LeaveEventMessage.class, this::forwardLeave)
                .match(WebSocketGatewayActor.ChatEventMessage.class, this::forwardChat)
                .match(RoomEmptyMessage.class, this::handleRoomEmpty)
                .build();
    }

    private void forwardJoin(final WebSocketGatewayActor.JoinEventMessage join) {
        String roomName = join.getTarget();
        if (StringUtils.isEmpty(roomName)) {
            return;
        }
        if (!roomActors.containsKey(roomName)) {
            ActorRef newRoomActor = getContext().actorOf(SingleRoomActor.props(roomName), "room-" + roomName);
            roomActors.put(roomName, newRoomActor);
        }
        ActorRef targetRoom = roomActors.get(roomName);
        targetRoom.tell(join, sender());
    }

    private void forwardLeave(final WebSocketGatewayActor.LeaveEventMessage leave) {
        if (leave.isGlobal()) {
            roomActors.values().forEach(roomActor -> roomActor.tell(leave, sender()));
            return;
        }
        String roomName = leave.getTarget();
        if (!roomActors.containsKey(roomName)) {
            return;
        }
        roomActors.get(roomName).tell(leave, sender());
    }

    private void forwardChat(final WebSocketGatewayActor.ChatEventMessage chat) {
        String roomName = chat.getTarget();
        if (!roomActors.containsKey(roomName)) {
            return;
        }
        roomActors.get(roomName).tell(chat, sender());
    }

    private void handleRoomEmpty(final RoomEmptyMessage empty) {
        String roomName = empty.getName();
        if (!roomActors.containsKey(roomName)) {
            return;
        }
        ActorRef emptyRoom = roomActors.remove(roomName);
        getContext().stop(emptyRoom);
    }

    public static Props props() {
        return Props.create(MasterRoomActor.class);
    }
}
