package com.github.michalbric.model;

import com.github.michalbric.gateway.WebSocketActionType;
import lombok.Data;

@Data(staticConstructor = "of")
public class ChatMessage {
    private final WebSocketActionType type;
    private final String authorName;
    private final String roomName;
    private final String text;
}
