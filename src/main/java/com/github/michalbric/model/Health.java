package com.github.michalbric.model;

import lombok.Data;

@Data(staticConstructor = "of")
public class Health {
    private final String state;
}
