package com.github.michalbric.name;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class NameActorsTest {
    private static ActorSystem system;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void testNameActors() throws InterruptedException {
        new TestKit(system) {{
            final ActorRef masterNameActor = system.actorOf(MasterNameActor.props());
            String userID = "test-id-1234";
            masterNameActor.tell(MasterNameActor.NameRequest.forUser(userID), getRef());
            Object response = receiveOne(duration("5 seconds"));
            assertTrue("Response is of type NameResponse", response instanceof MasterNameActor.NameResponse);
            MasterNameActor.NameResponse nameResponse = (MasterNameActor.NameResponse) response;
            assertEquals("The userIDs of the name response match", userID, nameResponse.getUserID());
            System.out.println(nameResponse.getName());

        }};

    }
}
